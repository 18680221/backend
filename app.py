
from flask import Flask, jsonify, request 
import pymongo
from bson import json_util
from bson.objectid import ObjectId
cliente=pymongo.MongoClient("mongo",27017)
db=cliente["pulque"]
negocios=db["negocios"]

app=Flask(__name__)
@app.route('/')
def home():
    items=negocios.find()
    json_negocios=[]
    item={}
    for negocio in items:
        item={
                "negocio":negocio["negocio"],
                "ubicacion":negocio["ubicacion"],
                "productos": negocio["productos"],
                "correo":negocio["correo"],
                "telefono": negocio["telefono"]
                }
        json_negocios.append(item)
    return jsonify({"response":json_negocios})
@app.route('/get/<id_r>')
def get(id_r):
    items=negocios.find()
    json_negocios=[]
    item={}
    for negocio in items:
        comentarios=[]
        suma=""
        numero=""
        if verSi(negocio, "comentarios"):
            comentarios=negocio["comentarios"]
        if verSi(negocio,"suma"):
            suma=negocio["suma"]
        if verSi(negocio, "numero"):
            numero=negocio["numero"]

        item={  "_id":str(negocio["_id"]),
                "negocio":negocio["negocio"],
                "ubicacion":negocio["ubicacion"],
                "productos": negocio["productos"],
                "correo":negocio["correo"],
                "telefono": negocio["telefono"],
                "comentarios":comentarios,
                "suma":suma,
                "numero":numero
                }

    
        json_negocios.append(item)
    print(json_negocios)
    return jsonify({"response":json_negocios})

def verSi(negocio, propiedad):
    try:
        negocio[propiedad]
        return True
    except:
        return False

@app.route('/add', methods=["POST"])
def add():
    data=request.get_json()
    
    negocios.insert_one(data)
    return jsonify({"response":"Added"})

@app.route('/update', methods=["POST"])
def update():
    data=request.get_json()
    negocios.update_one({'_id':ObjectId(data["_id"])}, {'$set':{'comentarios':data["comentarios"]}})
    return jsonify({"response":"update"})
 
@app.route('/updatenumero', methods=["POST"])
def updatenumero():
    data=request.get_json()
    negocios.update_one({'_id':ObjectId(data["_id"])}, {'$set':{'numero':data["numero"]}})
    return jsonify({"response":"update"})

@app.route('/updatesuma', methods=["POST"])
def updatesuma():
    data=request.get_json()
    negocios.update_one({'_id':ObjectId(data["_id"])}, {'$set':{'suma':data["suma"]}})
    return jsonify({"response":"update"})
        

